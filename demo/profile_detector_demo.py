# import the opencv library
import cv2
import torch
import torch.nn as nn
import urllib
import numpy as np
from PIL import Image
from torchvision import transforms
import torchvision.models as models

number_classes = 9
model = models.resnet101(pretrained=False)
num_ftrs = model.fc.in_features
model.fc = nn.Linear(num_ftrs, number_classes)
model.load_state_dict(torch.load('resnet101_40paramfreeze_79darsad.pt', map_location=torch.device('cpu')))
model.eval()


transforms = transforms.Compose(
    [
        transforms.Resize([480, 480]),
        transforms.ToTensor(),
        # transforms.CenterCrop(250),
        transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
    ])



# define a video capture object
vid = cv2.VideoCapture(2)
vid.set(3, 640)
vid.set(4, 480)
while (True):

    # Capture the video frame
    # by frame
    ret, frame = vid.read()
    frame = frame[:, :480]
    frame = cv2.flip(frame, 1)
    input_image = Image.fromarray(frame)

    input_tensor = transforms(input_image)
    input_batch = input_tensor.unsqueeze(0)  # create a mini-batch as expected by the model
    # move the input and model to GPU for speed if available
    with torch.no_grad():
        output = model(input_batch)
    # Tensor of shape 1000, with confidence scores over Imagenet's 1000 classes
    probabilities = nn.functional.softmax(output[0], dim=0)
    print(np.argmax(probabilities)+1)
    # Display the resulting frame
    cv2.imshow('frame', frame)

    # the 'q' button is set as the
    # quitting button you may use any
    # desired button of your choice
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# After the loop release the cap object
vid.release()
# Destroy all the windows
cv2.destroyAllWindows()