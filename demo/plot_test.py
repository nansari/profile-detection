import numpy as np
import matplotlib.pyplot as plt

plt.axis([0, 10, 0, 1])

y = np.array([])
i_all = np.array([])
for i in range(0, 10):
    y = np.concatenate((y, np.random.random(1)))
    i_all = np.concatenate((i_all, np.array([i])))
    plt.plot(i_all, y)
    plt.pause(0.1)
plt.show()
