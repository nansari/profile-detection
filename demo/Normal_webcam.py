## License: Apache 2.0. See LICENSE file in root directory.
## Copyright(c) 2017 Intel Corporation. All Rights Reserved.

#####################################################
##              Align Depth to Color               ##
#####################################################

# First import the library
import matplotlib.pyplot as plt

# Import Numpy for easy array manipulation
import numpy as np
# Import OpenCV for easy image rendering
import cv2
import torch
import torch.nn as nn
import urllib
import numpy as np
from PIL import Image
from torchvision import transforms
import torchvision.models as models

number_classes = 2
model = models.resnet101(pretrained=False)
num_ftrs = model.fc.in_features
model.fc = nn.Linear(num_ftrs, number_classes)
model.load_state_dict(torch.load('resnet101_TrainedForSample_5.pt', map_location=torch.device('cpu')))
model.eval()

transforms = transforms.Compose(
    [
        transforms.Resize([480, 480]),
        transforms.GaussianBlur(kernel_size=7, sigma=3),
        transforms.ToTensor(),
        # transforms.CenterCrop(250),
        transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
    ])


# Streaming loop
# filepath = r"C:\Users\Navid\Desktop\Navid\profil_codes\dataset_bgremoved\6"
cnt = 0
cap = cv2.VideoCapture(2)

for i in range(600):
    # Get frameset of color and depth
    ret, color_frame = cap.read()
    color_image = np.asanyarray(color_frame)
    im_out = color_image[:, :480, :]
    input_image = Image.fromarray(im_out)
    # plt.imshow(input_image)
    # plt.show()
    input_tensor = transforms(input_image)
    input_batch = input_tensor.unsqueeze(0)  # create a mini-batch as expected by the model
    # move the input and model to GPU for speed if available
    with torch.no_grad():
        output = model(input_batch)
    probabilities = nn.functional.softmax(output[0], dim=0)
    print(probabilities)
    print(np.argmax(probabilities))

    # cv2.namedWindow('Align Example', cv2.WINDOW_NORMAL)
    cv2.imshow('Align Example', im_out)
    key = cv2.waitKey(1)
    # Press esc or 'q' to close the image window
    if key & 0xFF == ord('q') or key == 27:
        cap.release()
        cv2.destroyAllWindows()
        break
