## License: Apache 2.0. See LICENSE file in root directory.
## Copyright(c) 2017 Intel Corporation. All Rights Reserved.

#####################################################
##              Align Depth to Color               ##
#####################################################

# First import the library
import pyrealsense2 as rs
# Import Numpy for easy array manipulation
import numpy as np
# Import OpenCV for easy image rendering
import cv2
import os


# Streaming loop
filepath = r"C:\Users\Navid\Desktop\Navid\profil_codes\final_setup_test\3"
cnt = 0
os.mkdir(filepath)
cap = cv2.VideoCapture(0)

for i in range(400):
    ret, color_frame = cap.read()
    color_image = np.asanyarray(color_frame)
    im_out = color_image[:480, -480:, :]
    if np.remainder(i,10)==0 and i>100:
        cnt = cnt+1
        filename = '%d.jpg' %(cnt)
        cv2.imwrite(filepath+'\\'+filename, im_out)

        im_out_gray = cv2.cvtColor(im_out, cv2.COLOR_BGR2GRAY)
        filename = 'g_%d.jpg' %(cnt)
        cv2.imwrite(filepath+'\\'+filename, im_out_gray)

        im_out_flip = cv2.flip(im_out, 1)
        filename = 'f_%d.jpg' %(cnt)
        cv2.imwrite(filepath+'\\'+filename, im_out_flip)

        # im_out_rotate = cv2.rotate(im_out, cv2.ROTATE_180)
        # filename = 'r_nobg%d.jpg' %(cnt)
        # cv2.imwrite(filepath+'\\'+filename, im_out_rotate)

    cv2.imshow('Align Example', im_out)
    key = cv2.waitKey(1)
    # Press esc or 'q' to close the image window
    if key & 0xFF == ord('q') or key == 27:
        cv2.destroyAllWindows()
        break

