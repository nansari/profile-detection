# Automatic Profile detection
#### NAVID ANSARI, Max Planck Institute for Informatics, Germany
#### VAHID BABAEI, Max Planck Institute for Informatics, Germany

#### Automatic Door and Window Profile Classification 


**Abstract:** 
This software uses deep learning for classifying different door and window profiles based on the shape of their cross-section.
![Profiles_example](Profiles_example.PNG)

**Problem:**
Due to the huge number of classes, classifying the profiles is a very challenging task 
[1]. In order to be useful in the industry our product should have the accuracy 
comparable to that of human classification. Moreover, since constantly new profile 
designs are introduced to the market, our proposed software should be flexible and 
allow adding new classes to the classifier.

**Current scientific state of the art:**
The industry currently relies on humans for profile classification. 

**Problem solution by the software:**
To address the large number of classes, we train a single neural network for each 
class. In this approach we will have a separate 0-1 classifier for each class, where 1 
means that the sample belongs to this class and 0 means it is from a different class.  
A new sample from a new unseen class, can be misclassified by some of our previously 
trained models. 
To avoid misclassification, we should feed the new sample to all the models. In case 
some of them misclassify it as 1, we should add a few instances of the new sample to 
its 0 class. Once we make sure none of the old models misclassify anymore, we can 
train a new model for this new sample. 
Since each model is independent of the others, increasing the number of classes will 
not affect the accuracy of the software. Moreover, since a single model does not have to 
see all the data, the size of each dataset remains manageable. Since the calculation 
time increases linearly with the number of classes, once we use the extra information of 
cargo and inventory, we can limit the calculation to the classes that we need in a 
specific time window. 

**Dataset and models:**
You can doenload the data set from [here](https://nextcloud.mpi-klsb.mpg.de/index.php/s/PBtP5Hp6JHiCCYn) and the Models from [here](https://nextcloud.mpi-klsb.mpg.de/index.php/s/KmZZF7Bpmji7DzZ).

**Requirements**
* Python 3.7.7
* numpy 1.21.3
* PyTorch 1.7.1+cu101
* matplotlib
* opencv 4.5.4-dev
* pillow 7.2.0

**Instructions:**
To be updated.

**Reference:**
[1] Yuan,	Z.,	Guo,	Z.,	Yu,	X.,	Wang,	X.,	&	Yang,	T.	(2020).	Accelerating	Deep	Learning	with Millions	of	Classes.	European	Conference	on	Computer	Vision,	711–726.
