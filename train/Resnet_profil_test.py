
from PIL import Image

import torch.nn as nn
import numpy as np
from torchvision import *
from torch.utils.data import Dataset, DataLoader


import matplotlib.pyplot as plt
def accuracy(out, labels):
    _,pred = torch.max(out, dim=1)
    return torch.sum(pred==labels).item()

# model = torch.hub.load('pytorch/vision:v0.10.0', 'resnet34', pretrained=True)
import torchvision.models as models
number_classes = 9

model = models.resnet18(pretrained=False)
num_ftrs = model.fc.in_features
model.fc = nn.Linear(num_ftrs, number_classes)
model.load_state_dict(torch.load('resnet.pt'))
model.eval()
transforms = transforms.Compose(
    [
        transforms.Resize([250, 250]),
        transforms.ToTensor(),
        # transforms.CenterCrop(250),
        transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
    ])
train_dataset = datasets.ImageFolder(root='./square_train', transform=transforms)
test_dataset = datasets.ImageFolder(root='./square_test', transform=transforms)
# Download an example image from the pytorch website

# url, filename = ("https://github.com/pytorch/hub/raw/master/images/dog.jpg", "dog.jpg")
# try: urllib.URLopener().retrieve(url, filename)
# except: urllib.request.urlretrieve(url, filename)
# filename = "test_1.jpg"
# sample execution (requires torchvision)

# input_image = Image.open(filename)


# input_tensor = preprocess(input_image)
# input_batch = input_tensor.unsqueeze(0) # create a mini-batch as expected by the model
model.to('cuda')
# move the input and model to GPU for speed if available
# if torch.cuda.is_available():
#     input_batch = input_batch.to('cuda')
#     model.to('cuda')

# with torch.no_grad():
#     outputs = model(input_batch)
# _, pred = torch.max(outputs.data, 1)
# # Tensor of shape 1000, with confidence scores over Imagenet's 1000 classes
# print(test_dataset.classes[pred])
# # The output has unnormalized scores. To get probabilities, you can run a softmax on it.
# probabilities = torch.nn.functional.softmax(outputs[0], dim=0)
# print(probabilities[pred])
test_dataloader = DataLoader(test_dataset, batch_size=48, shuffle=True)
net = model
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
batch_loss = 0
total_t = 0
correct_t = 0
val_loss = []
val_acc = []
criterion = nn.CrossEntropyLoss()
with torch.no_grad():
    net.eval()
    for data_t, target_t in (test_dataloader):
        data_t, target_t = data_t.to(device), target_t.to(device)
        outputs_t = net(data_t)
        loss_t = criterion(outputs_t, target_t)
        batch_loss += loss_t.item()
        _, pred_t = torch.max(outputs_t, dim=1)
        correct_t += torch.sum(pred_t == target_t).item()
        total_t += target_t.size(0)
    val_acc.append(100 * correct_t / total_t)
    val_loss.append(batch_loss / len(test_dataloader))
    print(f'validation loss: {np.mean(val_loss):.4f}, validation acc: {(100 * correct_t / total_t):.4f}\n')
